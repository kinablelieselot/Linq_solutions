<Query Kind="Statements">
  <Connection>
    <ID>6104ac11-dc8a-4b86-86a3-7f0eae207e8c</ID>
    <Persist>true</Persist>
    <Server>.\sqlexpress</Server>
    <Database>Northwind</Database>
    <ShowServer>true</ShowServer>
  </Connection>
</Query>

/* 1. Toon de productnamen waarvan we minder dan 20 UnitsInStock tellen, toon ook de bijbehorende categorienaam */
Products.Where(p => p.UnitsInStock <20).Select(p => new
{
	p.ProductName,
	p.Category.CategoryName
}).Dump();

/* 2. Toon de namen van de klanten waar in de ContactTitle het woord "sales" voorkomt en afkomstig zijn uit Italië */
Customers.Where(c => c.ContactTitle.Contains("sales") && c.Country == "Italy")
	.Dump();

/* 3. Toon de namen van de klanten waar in de ContactTitle het woord "sales" voorkomt en afkomstig zijn uit Italië, Frankrijk of Portugal*/
Customers.Where(c => c.ContactTitle.Contains("sales") && (c.Country == "Italy" ||c.Country =="France" || c.Country == "Portugal"))
	.Dump();

Customers.Where(c => c.ContactTitle.Contains("sales") && (new[] {"Italy","France","Portugal"}.Contains(c.Country)))
	.Dump();


/* 	4. Geef voor elke werknemer die een overste heeft (reportsto is ingevuld)
	   de voornaam en familienaam weer alsook hoeveel orders ze hebben behandeld in het meest recente jaar */
Employees.Where(e => e.ReportsTo != null)
.Select(e => new
{
	Voornaam = e.FirstName,
	Familienaam = e.LastName,
	AantalOrders = e.Orders.Where(o => o.OrderDate.Value.Year == Orders.Max(or => or.OrderDate.Value.Year)).Count()
})


.Dump();


/* 5. Toon de eerste letter en het aantal ProductNames dat met deze letter begint.
      We zien enkel de letters waarvoor er meer dan 2 producten zijn. */
Products.GroupBy(p => p.ProductName.Substring(0, 1))
	.Select(g => new
	{
		Letter = g.Key,
		Aantal = g.Count()
	}).Where( p => p.Aantal > 2)
	
	.Dump();
